<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to the challenges</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<h1>Application Status</h1>
			<ul>
				<li>App version: <g:meta name="app.version"/></li>
				<li>Grails version: <g:meta name="app.grails.version"/></li>
				<li>Groovy version: ${GroovySystem.getVersion()}</li>
				<li>JVM version: ${System.getProperty('java.version')}</li>
				<li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
				<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
				<li>Domains: ${grailsApplication.domainClasses.size()}</li>
				<li>Services: ${grailsApplication.serviceClasses.size()}</li>
				<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
			</ul>
			<h1>Installed Plugins</h1>
			<ul>
				<g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
					<li>${plugin.name} - ${plugin.version}</li>
				</g:each>
			</ul>
		</div>
		<div id="page-body" role="main">
			<h1>Gracias por participar en nuestros retos</h1>
			<p>Bienvenido, antes de nada agradecerte el tiempo y el esfuerzo que vas a emplear en la resolución de los siguientes problemas.</p>
            <p>Todas las instrucciones para cada uno de los problemas están en esta página</p>
			<div id="controller-list" role="navigation">
				<h2>Challenges:</h2>
				<ul>
                <li> <h3>#1 All test in green</h3>
                      <p>Si se ejecutan los 12 tests del proyecto 5 fallan :( </p>
                      <p> Tu misión es que los 12 se ejecuten correctamente.</p>
                </li>
                <li> <h3>#2 A new class on the block</h3>
                      <p>El paquete com.startupxplore.challenges.profiles contiene las clases de dominio que representan los eventos, organizaciones y personas que aparecen en Startupxplore.com, queremos añadir un nuevo tipo de entidad para recoger la información de las ofertas de trabajo. 
                      Cada oferta de trabajo tiene un conjunto de imágenes asociadas, así como enlaces a redes sociales, título, nombre, descripción. Básicamente es como el perfil de un evento pero sin fechas de principio y fin.</p>
                      <p>Tu misión es modelar e implementar dicha clase...feel free to refactor if you smell it.</p>
                </li>
                <li> <h3>#3 Messages from the boss</h3>
                      <p>La clase NotificationGenerator simula enviar 8 notificaciones por correo a 8 usuarios diferentes. Pero oh.. un momento, el mensaje es siempre el mismo! y el último becario se dejó sin implementar 4 tipos de notificaciones que no se están usando.</p>
                      <p>Tu misión es implementar los 4 nuevos tipos de notificación y que el main de NotificationGenerator envíe 1 mensaje distinto a cada usuario. Si, huele que apesta... uff... ¿si el jefe nos pide añadir más mensajes mañana, las loc de la clase tienden a infinito?</p>
                      
                </li>
                <li> <h3>#4 Walking down the tree</h3>
                      <p>Un árbol, binario para más detalles, lo puedes ver en la clase TreesController. ¿Sabías que se puede recorrer un árbol en diferentes órdenes?</p>
                      <p>Tu misión es hacer que cada acción del controlador renderice los nodos del arbol en el mismo orden que indican los números. Puedes usar el propio XML como estructura de datos, o implementar una propia si te resulta más sencillo.</p>
                </li>
                </ul>
			</div>
		</div>
	</body>
</html>
