package com.startupxplore.dto.notifications

public enum NotificationType {
    WELCOMEMAIL, FROMSSM, CONFIRMATIONLINKREMINDER, RESETPWD,
    
    INVITEUSER, INVITEORGANIZATION, ADDMOREDATA, ADDYOURIMAGES
    
        
}
