package com.startupxplore.challenges.profiles

import grails.test.mixin.*
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Asset)
class AssetSpec extends Specification {
    
    
    void "test asset save"() {
        when:
            def numberOfAssets = 4
            def assets = []
            (1..numberOfAssets).each{
                assets.add(new Asset(id:"assetid${it}", name: "image${it}", type: "PROFILE", overview: "image ${it} overview", url:"https://placeholdit.imgix.net/~text?txtsize=33&txt=${it}&w=350&h=150"))
            }
            
            assets*.save(flush: true)
        then:
            assert Asset.count() == numberOfAssets
    }
    
    
    void "test asset delete"() {
        when:
            def numberOfAssets = 4
            def assets = []
            (1..numberOfAssets).each{
                assets.add(new Asset(id:"assetid${it}", name: "image${it}", type: "PROFILE", overview: "image ${it} overview", url:"https://placeholdit.imgix.net/~text?txtsize=33&txt=${it}&w=350&h=150"))
            }
            
            assets*.save(flush: true)
            Asset.get(1).delete()
        then:
            assert Asset.count() == (numberOfAssets - 1)
    }
    
    void "test overview constraint"(){
        when:
            Asset asset = new Asset(id:"assetid", name: "image", type: "PROFILE", url:"https://placeholdit.imgix.net/~text?txtsize=33&txt=test&w=350&h=150")
        then:
            assertNull asset.save(flush: true)
            Asset.count() == 0
    }
    
    @Unroll("test url constraint #url")
    def "test url constraint"() {
        when:
            Asset asset = new Asset(id:"assetid", name: "image", type: "PROFILE", overview:"Una overview")
            asset.url = url
        then:
            asset.validate() == result
        where:
            url | result
            "invalid-url" | false
            "http://startupxplore.com/" | false
            "ttp:/invalid" | true
    }
}
