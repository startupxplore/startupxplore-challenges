package com.startupxplore.challenges.profiles

import spock.lang.Specification

abstract class ConstraintUnitSpec extends Specification {

    String getLongString(Integer length) {
        'a' * length
    }

    String getEmail(Boolean valid) {
        valid ? "nacho@startupxplore.com" : "nacho@m"
    }

    String getUrl(Boolean valid) {
        valid ? "https://startupxplore.com" : "http:/ww.helloworld.com"
    }
    

    void validateConstraints(obj, field, error) {
        def validated = obj.validate()
        if (error && error != 'valid') {
            assert !validated
            assert obj.errors[field]
            assert error == obj.errors[field]
        } else {
            assert !obj.errors[field]
        }
    }
}
